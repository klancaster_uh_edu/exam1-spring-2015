class Department < ActiveRecord::Base
  belongs_to :manager
  has_many :items
end
