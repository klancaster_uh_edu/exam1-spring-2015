# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Manager.create(:name => "John Doe")
Department.create(:name => "Clothing", :manager_id => Manager.first.id)
(1..10).each do |n|
  Item.create(:name => "Item #{n}", department_id: Department.first.id
  )
end